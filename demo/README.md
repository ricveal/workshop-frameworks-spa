# Frameworks SPA

This is the practical part of the workshop **"Introduction to Framewoks SPA"**.

The main idea is to create three easy implementations of a ToDo List application, each one
using one framework SPA (React, Angular and Vue).

## Node.js

The development environment of these frameworks is based on [Node.js](https://nodejs.org/es/).
Once you build the application and it is deployed in one static web server (such as Nginx or Apache),
Node.js is not required but, during the development work, is a required dependency.

Node.js is a virtual machine which works in Linux, Windows and MacOS without differences so
the development using these technology is cross-platform.

Please, consider using an LTS (Long Time Support) version for stable / "production-ready"
developments.

## CLI's 🛠

- [create-react-app](https://github.com/facebook/create-react-app)
- [angular-cli](https://github.com/angular/angular-cli)
- [vue-cli](https://github.com/vuejs/vue-cli)

You should follow the steps described in each tool to install them in your system.

## React

```bash
npx create-react-app react-todo
cd react-todo
npm start
```

## Angular

```bash
ng new --skip-tests true ng-todo
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? CSS
```

You can run the application in development mode with hot reloading using:

```bash
ng serve
```

## Vue

```bash
npm run serve
```

## Author

- [Ricardo Vega](https://ricveal.com) - @ricveal

## Special Thanks

Although these sample projects are not forks of [Sunil Sandhu](https://medium.com/@sunilsandhu) and
[Sam Borick](https://medium.com/@mibzman) work, is heavily inspired on it and these repositories and
posts have been taken as reference:

- [I created the exact same app in React and Vue](https://medium.com/javascript-in-plain-english/i-created-the-exact-same-app-in-react-and-vue-here-are-the-differences-e9a1ae8077fd)
- [I created the same app in React and Vue (Part 2: Angular)](https://medium.com/javascript-in-plain-english/i-created-the-exact-same-app-in-react-and-vue-part-2-angular-39b1aa289878)
- [React ToDo](https://github.com/sunil-sandhu/react-todo)
- [Angular ToDo](http://github.com/mibzman/ng-todo)
- [Vue ToDo](https://github.com/sunil-sandhu/vue-todo)
