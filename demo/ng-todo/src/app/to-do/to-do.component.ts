import { Component, OnInit } from '@angular/core';
import { ToDo } from './to-do';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {
  constructor() {}

  toDos: ToDo[] = [new ToDo('clean the house'), new ToDo('buy milk')];

  input = '';

  submitted = false;

  ngOnInit() {}

  deleteItem(i: number) {
    this.toDos.splice(i, 1);
  }

  addItem() {
    this.submitted = true;
    if (!this.input) {
      return;
    }
    this.toDos = this.toDos.concat(new ToDo(this.input));
    this.input = '';
  }

  shouldBeHidden(Valid: boolean, Pristine: boolean, Submitted: boolean): boolean {
    if (Submitted) {
      return Valid;
    } else {
      return Valid || Pristine;
    }
  }
}
