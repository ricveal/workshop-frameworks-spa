footer: **@ricveal** - 5 ABRIL 2019
autoscale: true
slidenumbers: true
footer-style: alignment(center)

[.slidenumbers: false]

## Introduction to Front **Frameworks** (Javascript)

#### Angular, React, Vue

---

[.list: #e47aa8, bullet-character( ), alignment(center)]

# `whoami` 👋

## ![inline 25%](assets/ricardo-vega.jpg) | **Ricardo Vega**

- 🌐 [ricveal.com](https://ricveal.com) | #️⃣ [@ricveal](https://twitter.com/ricveal)

<br>

- 💼 Arquitecto Senior Front

- Minsait (an Indra company) - HUB Valladolid

---

## Agenda

- Introduction
- SPA
- Frameworks SPA
- React
- Angular
- Vue
- Demo Time
- Q&A

---

# Intro

<br>
<br>

- `JavaScript !== Java`
- `JavaScript ~ ECMAScript`
- ES6 - June 2015
- DOM (Document Object Model)

![right](assets/js.png)

---

## MVC

![inline](assets/MVC.png)

---

# Single Page Application

Only one page which is changing dynamically its content according the user navigation and information.

- **User Experience**
- **JavaScript intense**
  - AJAX - API (REST)
  - Application logic in frontend

---

## Traditional Lifecycle

![inline](assets/traditional_lifecycle.png)

---

## SPA Lifecycle

![inline](assets/spa_lifecycle.png)

---

# SPA Frameworks

- Rendering reusable components
- Data Binding
  # State propagation

### Frameworks set **constrains** that **standardize** processes **restricting** what you and your team **can do.**

---

## (Web) Components

Modular, portable, replaceable, and reusable set of **well-defined** functionality that encapsulates its **implementation** and exporting it as a higher-level interface.

- Encapsulation
- Reusability
- Interoperability
- Application data and logic
- Visualization (\*)

---

## Data Binding

Syncing logic data of the application (state) with the graphic interface (view).

![inline](assets/data_binding.png)

---

## State propagation

Components promote **composition pattern**: components (parents) can be structured with other components (children).
State propagation (and changes) is shared between parents and children ➡️ **state propagation**.

- **Top ➡️ Down**: properties.
- **Down ➡️ Top**: events.
- **Top ↔️ Down**: double binding. Built-in capability of the framework to "_automate_" previous strategies. ⚠️Performance⚠️

---

# React

![inline](assets/react.png)

---

## Main characteristics

- Library (not a pure framework). M**V**C - View.
- Component-Based.
- Virtual DOM. Performance 🤖.
- JSX.
- Instagram -> Facebook.
- Visualization and logic in one file.

---

```js
class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: [], text: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.text.length) {
      return;
    }
    const newItem = {
      text: this.state.text,
      id: Date.now()
    };
    this.setState(state => ({
      items: state.items.concat(newItem),
      text: ""
    }));
  }

```

---

```js

render() {
  return (
    <div>
      <h3>TODO</h3>
      <TodoList items={this.state.items} />
      <form onSubmit={this.handleSubmit}>
        <label htmlFor="new-todo">What needs to be done?</label>
        <input
          id="new-todo"
          onChange={this.handleChange}
          value={this.state.text}
        />
        <button>Add #{this.state.items.length + 1}</button>
      </form>
    </div>
  );
}
}
```

---

```js
class TodoList extends React.Component {
  render() {
    return (
      <ul>
        {this.props.items.map(item => (
          <li key={item.id}>{item.text}</li>
        ))}
      </ul>
    );
  }
}

ReactDOM.render(<TodoApp />, document.getElementById("todos-example"));
```

---

## Components

In React, **everything are components**. Commonly they are classify in two groups:

- **Pure Components**: focus in rendering and render direct related logic. Could be functions.
- **High Order Components**: components without rendering. They adapt data and encapsulate logic which is propagated to pure components which translates this transformed data into view.

---

## State management

All components have:

- state (mutable)
- props (inmutable)

### Constrains

- The component can change its own state
- Data is propagated form parent components to children through properties (props).
- Props cannot be changed (inmutable)
- Top ➡️ Down flow.

---

### What happen when a child want to change the state of its parent ?

- The state of the parent is propagated to child as prop of the child so cannot be changed.
- Only the parent can change its own state.
- An event is thrown and the parent **reacts** to this event and process the change.

### State changes and Rendering

Both state and props changes **imply re-render** the component who experiment the change and its children.

---

### Flux

![inline](assets/flux.png)

---

### Redux

![inline](assets/redux.png)

---

### Building a framework

A complete framework needs some other libraries.

- **State management**: Redux, Mobx, etc
- **Fetching data**: Browser fetch, Axios, etc.
- ...

---

## Advance React

- Hooks
- Context
- Error Boundaries
- Fragments
- Portals

![right](assets/difficult.gif)

---

[.background-color: #e47aa8]

## Who is using it?

![inline](assets/react_uses.png)

---

# Angular

![inline](assets/angular.png)

---

## Main characteristics

- Typescript
- Modules
- Components
- Services. Dependency Injection.
- Not AngularJS 😦😦
- Google

---

## State management

- Services are _singletons_.
- Services as state stores. ⚠️Management⚠️.

There are some alternatives using Flux pattern / Redux approach. Bigger / enterprise applications.

---

### RxJS

> "RxJS is one of the hottest libraries in web development today. Offering a powerful, **functional approach** for dealing with events and with integration points into a growing number of frameworks, libraries, and utilities.

> But...
> Learning RxJS and **reactive programming is hard**. There's the multitude of concepts, large API surface, and fundamental shift in mindset from an imperative to declarative style."

- [Learn RxJS](https://www.learnrxjs.io/)

---

### Not today!

![inline](assets/hard.gif)

---

## One component, three files

- `app.component.ts`

```ts
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "My First Angular App!";
}
```

---

## Components

- The 3 strategies are implemented, including **double-binding**
- Each component has its own state mutable.
- `@Input()`: Some state property can be updated from the parent.
- `ChangeDetector`: Convert from props changes into state.
- Changes in state means re-rendering the component and its children.
- `@Output()`: Event emitter to parent.

---

#### Top ➡️ Down

- `component.ts`

```js
imageUrl = "http://mydomain.com/image.png";
```

- `component.html` - Template

```html
<div>
  <img [src]="imageUrl" />
  <img src="{{imageUrl}}" />
  <img src="http://mydomain.com/image.png" />
</div>
```

- `src` is an `@Input()` of the `img` component.

---

#### Down ➡️ Top

- `component.ts`

```js
clickHandler(param) {
    console.log(param);
}
```

- `component.html` - Template

```html
<div>
  <button (click)="clickHandler(event$)" />
</div>
```

- `click` is an `@Output()` of the `button` component.

---

#### Double-Binding: Down ↔️ Top

- `component.ts`

```js
name = "hola";
```

- `component.html` - Template

```html
<div>
  <textarea [(value)]="name" />
</div>
```

- `value` is an `@Input()` of the `textarea` component but is always and `@Output()` and its changes are synced in component.

---

### Some questions

If I have a property annotated with `@Input()`,

- can i change its value?
- its change will be reflected in the parent?
- how can I do that?

---

[.background-color: #e47aa8]

## Who is using it?

![inline](assets/angular_uses.png)

---

# Vue

![inline](assets/vue.png)

---

- /vjuː/
- Progressive framework designed to be be incrementally adoptable.
- **Reactive**
- Declarative rendering
- Lightweight
- Independent - Evan You

---

### Modular and incrementally adoptable

- Main focus on UI (view)
- We need extra libraries to work as a framework but **there are official options**:
  - Routing: vue-router
  - State: Vuex

Hybrid approach between being flexible and customizable and close stack solution.

---

## Similar to React in:

- Virtual DOM
- Component-Based.
- Reactive and composable view components
- Props and State
- Maintain focus in the core library, with concerns such as routing and global state management handled by companion libraries

But:

- JSX vs templates
- Component-Scoped CSS

---

## Similar to Angular

- Some inspiration in AngularJS (directives).
- Clearly separate concepts (logic, styles and templates).
- Double binding optional.
- Framework

---

## Components

- data: state
- methods: logic
- template (with directives)
- properties
- computed: logic properties with cache (if dependencies not change, result will be the same) 🧙. Setter and Getters
- watchers

---

```js
const vm = new Vue({
  el: "#example",
  data: {
    message: "Hello",
    firstName: "Foo",
    lastName: "Bar"
  },
  computed: {
    // a computed getter
    reversedMessage: function() {
      // `this` points to the vm instance
      return this.message
        .split("")
        .reverse()
        .join("");
    }
  },
  watch: {
    firstName: function(val) {
      this.fullName = val + " " + this.lastName;
    }
  }
});
```

---

## Single File Components (SFC)

By default, all is mixed in one only file per component with three parts clearly separated:

```js
<template>
  <div id="app">
    {{ message }}
  </div>
</template>

<script>
export default {
  data() {
    return {
      message: "Hello World"
    };
  },
  methods: {
    reverseMessage: function () {
        return this.message.split('').reverse().join('')
    }
    }
};
</script>

<style>
#app {
  font-size: 18px;
  font-family: "Roboto", sans-serif;
  color: blue;
}
</style>
```

---

## State Management

- Components have props (inmutable) and state (mutable)
- Props follow One-Way data flow, from parents to children. `v-bind:name="customer" === :name="customer"`
- Events as Down ➡️ Top flow: `v-on:click="onClick" === @click="onClick"`
- `v-model` directive to create two-way data bindings (forms)
- Vuex

---

[.list: #ffffff]

## Personal Opinion

- It is the youngest and learns from Angular and React.
- Tries to get the best from each one and focus on performance.
- Great great engineering.
- Less community 😞😞

---

[.background-color: #e47aa8]

## Who is using it?

![inline](assets/vue_uses.png)

---

# Demo time

![background](assets/coding.gif)

---

![background fit](assets/questions.gif)

---

[.background-color: #e47aa8]

# ¡Muchas gracias!
